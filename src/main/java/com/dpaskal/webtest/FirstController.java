package com.dpaskal.webtest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class FirstController{

    @GetMapping("/api/hello")
    public String Hello() {
        Date date = new Date();

        return "hello user. Current date is: " + date + "\n";
    }
}