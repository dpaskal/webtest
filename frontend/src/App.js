import React, {Component, useState, useEffect} from 'react';
import './App.css';

// api key - 5aca6438
const App = () => {
  const [message, setMessage] = useState("");
  const [counter, setCounter] = useState(0);

  const Person = (props) => {
    return (
      <>
      <div> Name: {props.name} </div>
      </>
    )
  }

  useEffect (() => {
    fetch('/api/hello')
      .then(response => response.text())
      .then(message => {
        setMessage(message);
      });
  }, [])
  return (
    <div className="App">
      <Person name={'Dan'} />
      <div>{message}</div>
      <button onClick={() => setCounter((prevCounter) => ++prevCounter)}>+</button>
      <div>{counter}</div>
      <button onClick={() => setCounter((prevCounter) => --prevCounter)}>-</button>
    </div>
  );
}

export default App;
